#!/bin/sh

# this assumes the first command line parameter is a path where 
# latex-tudesign_2016-03-01.zip
# tudfonts-tex_0.0.20090806.zip
# latex-tudesign-thesis_0020140703_B.zip (optional)
# are unpacked into one folder like achieved with
# - unzip -o '*.zip' -d your-path-here

cp -arv $1/texmf/* /usr/local/share/texmf/
texhash /usr/local/share/texmf
if test -d "$1/texmf/fonts/"; then
    echo "Some fonts are present in the install directory, proceeding..."
    echo "Enabling copyrighted fonts..."
    updmap-sys --enable Map 5ch.map
    updmap-sys --enable Map 5fp.map
    updmap-sys --enable Map 5sf.map
fi
