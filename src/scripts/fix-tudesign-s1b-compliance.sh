#!/bin/bash

rm -rf /usr/local/share/texmf/tex/latex/tuddesign/logo/*
cp src/fixed-a1b-graphics/tud_logo_a1b.pdf /usr/local/share/texmf/tex/latex/tuddesign/logo/tud_logo.pdf
rm -rf /usr/local/share/texmf/tex/latex/tuddesign/thesis/by-nc-nd*
cp src/fixed-a1b-graphics/by-nc-nd_a1b.pdf /usr/local/share/texmf/tex/latex/tuddesign/thesis/by-nc-nd.pdf
