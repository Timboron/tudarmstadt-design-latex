# latex-tudesign

[![pipeline status](https://gitlab.com/georg.jung/tudarmstadt-design-latex/badges/master/pipeline.svg)](https://gitlab.com/georg.jung/tudarmstadt-design-latex/commits/master)

## Getting Started

1. Dieses Repository forken
2. Folgende Dateien herunterladen und in den Ordner `src/corporatedesign` speichern:
    * [latex-tudesign_2016-03-01.zip](https://download.hrz.tu-darmstadt.de/protected/DezIF/Corporate_Design/LaTeX/latex-tudesign_2016-03-01.zip)
    * [latex-tudesign-thesis_0020140703_B.zip](https://download.hrz.tu-darmstadt.de/protected/DezIF/Corporate_Design/LaTeX/latex-tudesign-thesis_0020140703_B.zip)
    * [tudfonts-tex_0.0.20090806.zip](http://exp1.fkp.physik.tu-darmstadt.de/tuddesign/latex/tudfonts-tex/tudfonts-tex_0.0.20090806.zip)
      * Die Schriften sind nur aus dem Netzwerk der TU Darmstadt (auch via [VPN](https://www.hrz.tu-darmstadt.de/vpn)) abrufbar.
      * Alles funktioniert auch ohne die Schriften, nur sehen die PDFs dann anders aus und verwenden auf dem System vorhandene Schriften.
3. `git add src/corporatedesign/* && git commit -m "add tu corporate design files" && git push`
4. Fertig. Die Beispieldatei `thesis.tex` anpassen. GitLab CI funktioniert auch in privaten Repositories sofort und kostenlos. Ein aktuelles PDF sollte in ein paar Minuten als *Artefakt* unter `CI / CD` (Link auf der linken Seite) abrufbar sein.
    * Nun sollten noch die (absoluten) Links unter [Aktuelle Versionen](#aktuelle-versionen) und das CI-Badge ganz am Anfang des ReadMes (Achtung, 2x anpassen: Bild und Link) angepasst werden, damit sie auch im geforkten Reppository wie erwartet funktionieren. Insgesamt sind 5 Vorkommen anzupassen. Am einfachsten kann man mit einem Texteditor `https://gitlab.com/georg.jung/tudarmstadt-design-latex/` entsprechend ersetzen.

Als Fallback funktioniert der Fork dieses Repositories auch direkt, ohne dass die obigen Schritte ausgeführt wurden. Dann wird auch ein PDF kompiliert usw., allerdings sind dann die Schriften falsch und es wird die alte Version des Corporate Designs verwendet. Leider ist das nicht anders realisierbar, ohne das Copyright der entsprechenden Inhalte zu verletzen.

## Aktuelle Versionen

* **[PDF](https://gitlab.com/georg.jung/tudarmstadt-design-latex/-/jobs/artifacts/master/raw/compile/thesis.pdf?job=install-tudesign-and-compile)** - Das finale PDF, wie es abgegeben und ausgedruckt werden kann.
* [HTML](https://gitlab.com/georg.jung/tudarmstadt-design-latex/-/jobs/artifacts/master/raw/converted/thesis.html?job=create-other-formats) - für die einfache Betrachtung im Browser und am Smartphone
* [DOCX](https://gitlab.com/georg.jung/tudarmstadt-design-latex/-/jobs/artifacts/master/raw/converted/thesis.docx?job=create-other-formats) - um Microsoft Word für die Rechtschreibprüfung nutzen zu können

## Facts

* automatische Übersetzung nach PDF, HTML, DOCX
* inkl. Fonts und allen Abhängigkeiten
* nach Möglichkeit PDF/A-1b-konform
* TUbama geeignet (s.u.)
* LuaLaTeX

## Beschreibung

Dieses Repository unterstützt bei der Erstellung von Dokumenten und insbesondere Abschlussarbeiten im dem Corporate Design der TU-Darmstadt. Einerseits sind hier diverse nötige Schritte und Tipps dokumentiert. Andererseits kompiliert das Repository vollautomatisch alle im Hauptverzeichnis liegenden `.tex`-Dateien nach Möglichkeit PDF/A-1b-konform. Links zum entsprechenden Ergebis stehen [am Anfang dieser Datei](#aktuelle-versionen). Natürlich ist dieses Repository nicht *nur* für Abschlussarbeiten geeignet. Wenn andere `.tex`-Dateien wie Präsentationen, Reports oder Übungen hier abgelegt werden, werden auch diese wie hier beschrieben übersetzt. Dann macht es potenziell Sinn, [die PDF/A-1b-Validierung zu deaktiveren](#pdfa-validierung-deaktivieren).

[TUbama](https://tubama.ulb.tu-darmstadt.de/) behauptet, das PDF/A-1b-Format vorauszusetzen, führt aber nur eine sehr grobe Prüfung durch. Dieses Repository kann *tatsächlich* konforme PDFs erstellen und validiert das auch automatisiert. Falls diese Prüfung fehlschlägt (bspw. nach dem Einbinden externer PDFs oder Grafiken), stehen die Chancen trotzdem gut, dass TUbama die Datei akzeptiert.

Das Ziel dieses Repositories ist es insbesondere nicht nur, PDFs im TU-Design zu erstellen (was auch mit dem unten verlinkten Docker-Image möglich ist), sondern diese auch direkt PDF/A-1b-konform zu erstellen. Das schränkt die verwendbaren PDF-Funktionalitäten ein. Beispielsweise ist die PDF-Version dadurch auf Version 1.4 beschränkt. Leider ist LaTeX nicht optimal geeignet, um PDF/A-Dateien zu erstellen. Insbesondere können mit dem Code in diesem Repository durchaus auch nicht standardkonforme PDFs erzeugt werden, wenn entsprechende Grafiken oder andere PDFs eingebunden werden. Dieses Projekt validiert echte PDF/A-1b-Konformität automatisiert. Für dieses Repository wird das modernere LuaLaTeX zur Kompilierung verwendet, während pdfLaTeX an der TU Darmstadt verbreiteter ist. Die Kompilierung dieses Beispiels gelingt mit pdfLaTeX genauso, das Ergebnis ist dann allerdings nicht vollständig PDF/A-1b-konform. Im Branch `pdflatex` ist die entsprechende Kompilierung umgesetzt.

## PDF/A-Validierung und Notfalloptionen

Der Dateicheck von TUbama prüft nur sehr grob auf PDF/A-1b-Konformität. Man kann auch Dateien mit so manchen Fehlern problemlos einreichen. Deshalb sollte man den Dateicheck von TUbama ausprobieren, bevor man kompliziertere Maßnahmen ergreift. Da nicht klar ist, welche Merkmale TUbama prüft, versucht dieses Repository trotzdem, echte Konformität zu schaffen.

Besonders einfach und unkompliziert nutzbar ist der [3-Heights Online Validator](https://www.pdf-online.com/osa/validate.aspx). Mehr Details, die bei der Fehlerbehebung hilfreich sein können, liefert [veraPDF](https://verapdf.org/home/). Die meisten Möglichkeiten bietet Adobe Acrobat Pro, was eine Lizenz (Studentenversion verfügbar, rund 80€; oder Testversion) erfodert. Dort gibt es ein Tool, das *Preflight* heißt und entsprechend testet. Acrobat Pro kann auch kleinere Konformitätsprobleme beheben.

Im Notfall kann PDF/A-Konformität durch "Drucken" mit einem virtuellen PDF-Drucker erreicht werden. Dafür kommt neben Acrobat Pro auch bspw. [PDF24](http://de.pdf24.org/) in Frage.

### PDF/A-Validierung deaktivieren

Dazu muss der `validate-pdf-a-1b`-Job aus der `.gitlab-ci.yml`-Datei entfernt werden. Einfach die folgenden Zeilen löschen:

    validate-pdf-a-1b:
      stage: validation
      dependencies:
        - install-tudesign-and-compile
      script:
        - apt-get update && apt-get install curl -y
        - cd compile
        - bash ./../src/scripts/validate-compiled-pdfs.sh
      artifacts:
        paths:
        - "./compile/*-validation.json"
        expire_in: 1 week
        when: always

## PDF/A-Troubleshooting

* Steht man vor Problemen mit Colorspaces etc., lassen sich diese häufig durch den Umstieg auf eine aktuellere texlive-Version beheben. Insbesondere sind auch die texlive-Versionen in aktuellen stable-Releases von Distributionen nicht unbedingt aktuell genug. Dieses Repository arbeitet daher mit einem Docker-Container auf Basis von debian testing.
* Ein weiteres Problem mit Colorspaces können eingebundene Grafiken darstellen. Interessanterweise sind ausgerechnet die beiden Grafiken `by-nc-nd` und `tud_logo` aus der offiziellen TU-Design-Vorlage nicht so einfach mit dem PDF/A-1b-Standard vereinbar. Daher habe ich beide Grafiken manuell in PDF/A-1b-Versionen konvertiert und entsprechende Installationsschritte, die die Originale ersetzen, im Repository eingefügt.
* Fehler in den XMP-Metadaten sind auch eine mögliche Ursache. Die genannten Validierungstools liefern hierzu auch Details. Hilfreich kann es auch sein, die PDF-Datei einfach bspw. mit Notepad++ zu öffnen, da die XMP-Daten direkt menschenlesbar in die Datei geschrieben werden. Bei der Verarbeitung der Metadaten auftretende LaTeX-Fehler werden teilweise nicht beim Kompilieren bemängelt, sondern die Fehlermeldung wird einfach als Teil des XMP-XMLs in das PDF geschrieben.
* [Blog-Post mit weiteren Tips](http://kulturreste.blogspot.com/2014/06/grrrr-oder-wie-man-mit-latex-vielleicht.html)
* Anleitung der TU Berlin lesen!

## Weiterführende Links
* [Offizielle Seite der TU](https://www.intern.tu-darmstadt.de/arbeitsmittel/corporate_design_vorlagen/index.de.jsp)
  * Das Paket *tudesign* hier ist aktueller (2016-03-01) als das auf den Seiten der Physiker.
  * Das aktuelle Paket von hier wird im Repo verwendet.
* [TU Design Seite bei der Festkörperphysik](http://exp1.fkp.physik.tu-darmstadt.de/tuddesign/)
  * ursprüngliche Ersteller der LaTeX-Klassen, verschiedene veraltete Installationsanleitungen
* [Sehr lesenswerte Anleitung der TU Berlin zu PDF/A](https://www.ub.tu-berlin.de/fileadmin/pdf/Verlag/UV_pdfaDE.pdf)
* [Docker-Image, das our-of-the-box funktioniert](https://hub.docker.com/r/jfornoff/latex-tuddesign/)
  * ziemlich alt, nicht mehr gewartet
  * durch das Alter der verwendeten Pakete gibt es Probleme bei der PDF/A-Erzeugung.
* [Arch Linux Paket](https://aur.archlinux.org/packages/latex-tuddesign/)
  * nicht getestet
* [TUbama](https://tubama.ulb.tu-darmstadt.de/)
* [pdfx Dokumentation](http://mirrors.ctan.org/macros/latex/contrib/pdfx/pdfx.pdf)
  * Das TeX Paket, das die Konformität herstellt.
  * Die Dokumentation ist sehr hilfreich und geht auf diverse Fälle ein.
* [StackOverflow: How can I test a PDF document if it is PDF/A compliant?](https://stackoverflow.com/a/2367116/1200847)
  * [3-Heights Online Validator](https://www.pdf-online.com/osa/validate.aspx)
  * [veraPDF](https://verapdf.org/home/) - Gratis Open-Source Offline PDF/A Validierung
* [Docker Paket aergus/latex](https://hub.docker.com/r/aergus/latex)
  * basiert auf Debian testing, damit eine möglichst neue texlive-Version verwendet wird.
  * texlive-full
* [Blog-Post mit Troubleshooting-Tips](http://kulturreste.blogspot.com/2014/06/grrrr-oder-wie-man-mit-latex-vielleicht.html)

## ToDo
* Mathematische Formeln werden in HTML falsch dargestellt. Die MathJax-Einbindung via PanDoc ist nicht völlig trivial.

## Lizenzhinweise

Bitte beachten Sie, dass das Corporate Design der TU Darmstadt genauso wie auch das hier verwendete Logo der TU Darmstadt nur nach den Regeln des jeweiligen Rechteinhabers verwendet werden dürfen. Insbesondere stehen die entsprechenden Inhalte nicht unter der MIT-Lizenz, die für das restliche Projekt gilt. Weitere Informationen finden Sie auf den [Seiten der TU Darmstadt zum Thema](https://www.intern.tu-darmstadt.de/arbeitsmittel/corporate_design_vorlagen/index.de.jsp).